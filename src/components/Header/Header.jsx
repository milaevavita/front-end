import React from "react";
import "./Header.sass";
import logo from "./logo.svg";
import item from "./user-superstar-2x.jpg";
import out from "./sign-out.svg";

const Header = () => {
  return (
    <header>
      <div className="container">
        <div className="row">
          <div className="col-logo">
            <img src={logo} alt="logo" className="logo" />
          </div>
          <div className="col-menu">
            <a href="#" className="menu-items" target="_blank">
              About me{" "}
            </a>
            <a href="#" className="menu-items">
              Relationships
            </a>
            <a href="#" className="menu-items">
              Requirements{" "}
            </a>
            <a href="#" className="menu-items">
              Users{" "}
            </a>
            <a href="#" className="menu-items">
              Sign Up{" "}
            </a>
          </div>
          <div className="col-in">
            <div className="col1">
              <p className="in-name">Superstar</p>
              <a href="mailto:Superstar@gmail.com" className="in-mail">
                <span className="in-email">Superstar@gmail.com</span>
              </a>
            </div>
            <img src={item} alt="item" className="in-item" />
            <a href="#" className="in-out">
              <img src={out} alt="" className="in-sign" />
            </a>
          </div>
        </div>
      </div>
    </header>
  );
};
export default Header;
