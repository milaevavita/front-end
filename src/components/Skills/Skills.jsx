import React from "react";
import "./Skills.sass";
import html from "./html.png";
import css from "./css.png";
import js from "./js.png";


const Skills = () =>{
  return(
    <section id="skills">
      <div className="container">
        <h2 className="skills-title">About my relationships with web-development</h2>
        <div className="skills-cols">
          <div className="skills-col">
            <img src={html} alt="html" className="skills-item"/>
            <h6 className="skills-subtitle">I'm in love with HTML</h6>
            <p className="skills-txt">Hypertext Markup Language (HTML) is the standard markup language for creating web pages and web applications.</p>
          </div>
          <div className="skills-col">
          <img src={css} alt="css" className="skills-item"/>
            <h6 className="skills-subtitle">CSS is my best friend</h6>
            <p className="skills-txt">Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a document written in a markup language like HTML.</p>
          </div>
          <div className="skills-col">
          <img src={js} alt="JavaScript" className="skills-item"/>
            <h6 className="skills-subtitle">JavaScript is my passion</h6>
            <p className="skills-txt">JavaScript is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.</p>
          </div>
        </div>
      </div>
      
    </section>
  )
}

export default Skills;