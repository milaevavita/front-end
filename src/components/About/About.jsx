import React from "react";
import "./About.sass";
import bg from "./alex-kotliarskyi-361099-unsplash.png";

const About = () => {
  return (
    <section id="about">
      <div className="about-row" >
        <img src={bg} alt="team" className="about-team"/>
      </div>
      <div className="container">
        <div className="about-col">
          <h1 className="about-title">
            Test assignment for Frontend Developer position
          </h1>
          <p className="about-txt">
            We kindly remind you that your test assignment should be submitted
            as a link to github/bitbucket repository. Please be patient, we
            consider and respond to every application that meets minimum
            requirements. We look forward to your submission. Good luck!
          </p>
          <button className="about-btn">Sign Up</button>
        </div>
      </div>
    </section>
  );
};
export default About;
