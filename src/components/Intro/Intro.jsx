import React from "react";
import "./Intro.sass";
import intro from "./intro.png";


const Intro = ()=> {
  return (
<section id="intro">
  <div className="container">
    <h2 className="intro-title">Let's get acquainted</h2>
    <div className="intro-cols">
      <div className="intro-col1">
        <img src={intro} alt="Let's get acquainted" className="intro-item"/>
      </div>
      <div className="intro-col2">
        <h6 className="intro-subtitle">I am cool frontend developer</h6>
        <p className="intro-txt">
          When real users have a slow experience on mobile, they're much less likely 
          to find what they are looking for or purchase from you in the future. For 
          many sites this equates to a huge missed opportunity, especially when more
           than half of visits are abandoned if a mobile page takes over 3 seconds to load.</p>
           <br/>
        <p className="intro-txt">Last week, Google Search and Ads teams announced two new speed
         initiatives to help improve user-experience on the web. </p>
        <a href="#" className="intro-btn">Sign Up</a>
      </div>
    </div>
  </div>
</section>
  )
}

export default Intro;