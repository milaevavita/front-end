import React from "react";
import "./General.sass";

const General = () => {
  return (
    <section id="general">
      <div className="general-rows">
        <img src="" alt="" className="general-bg" />
      </div>
      <div className="container">
        <h2 className="general-title">
          General requirements for the test task
        </h2>
        <div className="general-cols">
          <div className="general-col">
            <p className="general-txt">
              Users want to find answers to their questions quickly and data
              shows that people really care about how quickly their pages load.
              The Search team announced speed would be a ranking signal for
              desktop searches in 2010 and as of this month (July 2018), page
              speed will be a ranking factor for mobile searches too.
              <br />
              If you're a developer working on a site, now is a good time to
              evaluate your performance using our speed tools. Think about how
              performance affects the user experience of your pages and consider
              measuring a variety of real-world user-centric performance
              metrics.
              <br />
              Are you shipping too much JavaScript? Too many images? Images and
              JavaScript are the most significant contributors to the page
              weight that affect page load time based on data from HTTP Archive
              and the Chrome User Experience Report - our public dataset for key
              UX metrics as experienced by Chrome users under real-world
              conditions.
            </p>
          </div>
          <div className="general-col">
            <img src="" alt="" className="general-item" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default General;
