import React from "react";
import "./App.sass";
import "./fonts/Fonts.css";
import "./normalize.css";
import Header from "./components/Header/Header";
import About from "./components/About/About";
import Intro from "./components/Intro/Intro";
import Skills from "./components/Skills/Skills";
import General from "./components/General/General";

function App() {
  return (
    <div className="App">
      <Header />
      <About />
      <Intro />
      <Skills />
      <General />
    </div>
  );
}

export default App;
